
import 'package:boats/src/models/boat.dart';

class Data {
  static List<Boats> boats = [
  Boats(title: 'X22 Lift', by: 'Mastercraft', img: 'assets/boat1.png'),
  Boats(title: 'X24 Fun', by: 'Mastercraft', img: 'assets/boat2.png'),
  Boats(title: 'Lagoon L50', by: 'Neokraft', img: 'assets/boat3.png'),
  Boats(title: 'Beneteau First 14', by: 'Neokraft', img: 'assets/boat4.png'),
];

static List<String> gallery = [
  'assets/sea1.jpg',
  'assets/sea2.jpg',
  'assets/sea3.jpg',
  'assets/sea4.jpg'
];
}